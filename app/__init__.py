import os
from flask import Flask
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config.update(
    TEMPLATES_AUTO_RELOAD=True
)


from app.deamon import controller