# -*- coding: utf-8 -*-
import hashlib
import os

from flask import render_template, request

from app import app
from app.other_function.other_functions import save_file, delete_file, download_file, get_format, create_dir


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', title='Хранилище файлов')

@app.route('/api/file', methods=['POST'])
def work_for_file():
    data = request.form
    call_def = tuple((i for i in data))
    items = tuple((v for i,v in data.items()))
    if not call_def:
        return save_file(request)
    elif call_def[0] == "delete":
        return delete_file(items[0])
    elif call_def[0] == "download":
        return download_file(items[0])
    return "ERROR"