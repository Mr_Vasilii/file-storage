# -*- coding: utf-8 -*-
from app import app
from app.other_function.other_functions import create_dir

if __name__ == '__main__':
    create_dir("app/static/store/")
    app.run(host='0.0.0.0', port=5000)